/*
 *  JLib - Jacob's Library.
 *  Copyright (C) 2003, 2004  Juan Carlos Seijo P�rez
 * 
 *  This library is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU Library General Public
 *  License as published by the Free Software Foundation; either
 *  version 2 of the License, or (at your option) any later version.
 * 
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Library General Public License for more details.
 * 
 *  You should have received a copy of the GNU Library General Public
 *  License along with this library; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 *  Juan Carlos Seijo P�rez
 *  jacob@mainreactor.net
 */

/** Includes JLib files.
 * @file JLib.h
 * @author Juan Carlos Seijo
 * @date 07/03/2004
 * @version 0.0.1 - 07/03/2004 - First version.
 */

#ifndef _JLIB_INCLUDED
#define _JLIB_INCLUDED

#include <JLib/Util/JTypes.h>

#include <JLib/Graphics/JASEFormat.h>
#include <JLib/Graphics/JASELoader.h>
#include <JLib/Graphics/JControl.h>
#include <JLib/Graphics/JDrawable.h>
#include <JLib/Graphics/JGLAxes.h>
#include <JLib/Graphics/JGLCamera.h>
#include <JLib/Graphics/JGLColor.h>
#include <JLib/Graphics/JGLConsole.h>
#include <JLib/Graphics/JGLConsoleFont.h>
#include <JLib/Graphics/JGLGrid.h>
#include <JLib/Graphics/JGLLight.h>
#include <JLib/Graphics/JGLText.h>
#include <JLib/Graphics/JGLTexture.h>
#include <JLib/Graphics/JGLVector.h>
#include <JLib/Graphics/JImage.h>
#include <JLib/Graphics/JImageControl.h>
#include <JLib/Graphics/JImageSprite.h>
#include <JLib/Graphics/JSprite.h>
#include <JLib/Graphics/JTextMenu.h>
#include <JLib/Math/J2DPolygon.h>
#include <JLib/Math/J2DSegment.h>
#include <JLib/Math/J2DVector.h>
#include <JLib/Math/JBasis.h>
#include <JLib/Math/JCoordAxes.h>
#include <JLib/Math/JMath.h>
#include <JLib/Math/JMatrix.h>
#include <JLib/Math/JVector.h>
#include <JLib/Physics/JRigidBodyState.h>
#include <JLib/Sound/JMixer.h>
#include <JLib/Util/JApp.h>
#include <JLib/Util/JCompatibility.h>
#include <JLib/Util/JDebug.h>
#include <JLib/Util/JFile.h>
#include <JLib/Util/JFLTK.h>
#include <JLib/Util/JFS.h>
#include <JLib/Util/JGLApp.h>
#include <JLib/Util/JLoadSave.h>
#include <JLib/Util/JObject.h>
#include <JLib/Util/JString.h>
#include <JLib/Util/JTextFile.h>
#include <JLib/Util/JTextUtil.h>
#include <JLib/Util/JTimer.h>
#include <JLib/Util/JTree.h>
#include <JLib/Util/JUtil.h>
#include <JLib/Util/JFont.h>

#endif  // _JLIB_INCLUDED
