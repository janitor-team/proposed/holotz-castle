/*
 *  JLib - Jacob's Library.
 *  Copyright (C) 2003, 2004  Juan Carlos Seijo P�rez
 * 
 *  This library is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU Library General Public
 *  License as published by the Free Software Foundation; either
 *  version 2 of the License, or (at your option) any later version.
 * 
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Library General Public License for more details.
 * 
 *  You should have received a copy of the GNU Library General Public
 *  License along with this library; if not, write to the Free
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 *  Juan Carlos Seijo P�rez
 *  jacob@mainreactor.net
 */

/** Menu en pantalla compuesto de cadenas de texto.
 * @file    JTextMenu.h
 * @author  Juan Carlos Seijo P�rez
 * @date    28/03/2004
 * @version 0.0.1 - 28/03/2004 - Primera versi�n.
 */

#ifndef _JTEXTMENU_INCLUDED
#define _JTEXTMENU_INCLUDED

#include <JLib/Graphics/JDrawable.h>
#include <JLib/Graphics/JImage.h>
#include <JLib/Graphics/JFont.h>
#include <JLib/Util/JApp.h>
#include <JLib/Util/JString.h>
#include <JLib/Util/JTree.h>
#include <JLib/Util/JTimer.h>

/** Modo de renderizado de la fuente del men�.
 */
typedef enum 
{
	JTEXTMENU_SOLID = 1,                  /**< S�lido con colorkey. */
	JTEXTMENU_SHADED,                     /**< Antialiasing con fondo s�lido. */
	JTEXTMENU_BLENDED,                    /**< Antialiasing con fondo transparente. */
} JTextMenuRenderStyle;

/** Distribuci�n del men�.
 */
typedef enum 
{
	JTEXTMENU_LEFT = 1,                   /**< Alineado a la izquierda. */
	JTEXTMENU_RIGHT,                      /**< Alineado a la derecha. */
	JTEXTMENU_CENTER,                     /**< Centrado. */
	JTEXTMENU_SAMELINE,                   /**< Todas las opciones en la misma l�nea. */
	JTEXTMENU_UP,                         /**< Alineado arriba. */
	JTEXTMENU_DOWN,                       /**< Alineado abajo. */
} JTextMenuLayout;

/** Estructura de configuraci�n del men�.
 */
struct JTextMenuConfig
{
	JFont * font;                         /**< Fuente a usar. */
	SDL_Color color;                      /**< Color de fuente. */
	SDL_Color backColor;                  /**< Color de fondo (JTEXTMENU_SHADED). */
	SDL_Color hiColor;                    /**< Color resaltado. */
	SDL_Color hiBackColor;                /**< Color de fondo resaltado (JTEXTMENU_SHADED). */
	s32 lineDistance;                     /**< Separaci�n entre l�neas (-1 para la predeterminada de la fuente). */
	JTextMenuLayout layout;               /**< Distribuci�n del men� en horizontal. */
	JTextMenuLayout layoutV;              /**< Distribuci�n del men� en vertical. */
	JTextMenuRenderStyle renderMode;      /**< Modo de renderizado. */
	bool trackMouse;                      /**< Indica si puede manejarse con rat�n. */
	bool trackKeyboard;                   /**< Indica si puede manejarse con teclado. */
	bool trackJoystick;                   /**< Indica si puede manejarse con joystick/pad. */
	bool autoEnter;                       /**< Indica si desciende si no hay acci�n asociada. */
};

/** Opci�n de men� b�sica.
 */
class JTextMenuEntry
{
 protected:
 public:
	JImage *image;                        /**< Imagen asociada a la opci�n. */
	JImage *hiImage;                      /**< Imagen asociada a la opci�n resaltada. */

	JString text;                         /**< Texto de la opci�n de men�. */
	void (*Action)(void *data);           /**< Acci�n asociada a la opci�n de men�. */
	void *data;                           /**< Datos asociados a la opci�n de men�. */

 public:
	JTextMenuEntry(const JString &str, void (*pAct)(void *) = 0, void *actionData = 0)
	: image(0), hiImage(0), text(str), Action(pAct), data(actionData)
	{}

	/** Selecciona la opci�n de men�, llamando a la callback asociada, si existe.
	 * @return <b>true</b> en caso de haber acci�n asociada, <b>false</b> si no.
	 */
	bool Select();

	/** Devuelve la imagen asociada.
	 * @return La imagen asociada.
	 */
	JImage * Image() {return image;}

	/** Devuelve la imagen asociada al estado resaltado.
	 * @return La imagen asociada al estado resaltado.
	 */
	JImage * HiImage() {return hiImage;}

	/** Renderiza la opci�n de men� con la configuraci�n dada.
	 * @param  cfg Configuraci�n de men�.
	 * @return <b>true</b> Si todo fue bien, <b>false</b> si no se pudo renderizar.
	 */
	bool Init(JTextMenuConfig &cfg);

	/** Devuelve el texto de esta opci�n.
	 */
	const JString& Text() {return text;}

	/** Destruye el objeto y libera las imagenes asociadas.
	 */
	void Destroy()
	{
		JDELETE(image);
		JDELETE(hiImage);
	}

	/** Destruye el objeto y libera las imagenes asociadas.
	 */
	virtual ~JTextMenuEntry()
	{
		Destroy();
	}
};

/** Encapsula un men� de texto en pantalla.
 */
class JTextMenu : public JDrawable
{
	JTree<JTextMenuEntry *> options;      /**< �rbol de opciones de men�. */
	JTree<JTextMenuEntry *>::Iterator *curOption; /**< Opci�n actual. */
	JTextMenuConfig config;               /**< Par�metros de configuraci�n del men�. */
	s32 maxW;                             /**< Anchura m�xima. */
	s32 maxH;                             /**< Altura m�xima. */
	
	/** M�todo recursivo de renderizaci�n de las opciones del men�.
	 * @param it Iterador de la opci�n actual.
	 * @return <b>true</b> Si todo fue bien, <b>false</b> si no hay memoria.
	 */
	bool RenderBranch(JTree<JTextMenuEntry *>::Iterator *it);

	/** Aplica la distribuci�n de men�.
	 */
	void ApplyLayout(JTree<JTextMenuEntry *>::Iterator *it);

 public:
	/** Crea un men� vac�o. Init debe ser llamada antes de utilizar el objeto.
	 */
	JTextMenu();

	/** Inicializa el men� con la configuraci�n dada. Esta funci�n debe
	 * ser llamada despu�s de haber rellenado el �rbol del men�.
	 * @param  cfg Configuraci�n del men�.
	 * @return <b>true</b> Si todo fue bien, <b>false</b> si no se encontr� la
	 * fuente dada.
	 */
	bool Init(JTextMenuConfig &cfg);

	/** Devuelve el iterador de opci�n actual.
	 * @return Iterador de opci�n actual.
	 */
	JTree<JTextMenuEntry *>::Iterator * Menu() {return curOption;}

	/** Funci�n de dibujo del men�.
	 */
	virtual void Draw();

	/** Procesa tecla arriba
	 */
	virtual void TrackKeyboard(SDL_keysym key);

	/** Procesa movimiento de mouse
	 */
	virtual void TrackMouse(s32 bt, s32 x, s32 y);

	/** Devuelve la anchura m�xima del men�.
	 * @return Anchura m�xima del men�.
	 */
	s32 MaxW() {return maxW;}

	/** Devuelve la altura m�xima del men�.
	 * @return Altura m�xima del men�.
	 */
	s32 MaxH() {return maxH;}

	/** Destruye el objeto y libera la memoria asociada.
	 */
	virtual ~JTextMenu()
	{
		for (options.Begin(); !options.End(); options.Next())
		{
			JDELETE(options.Cur());
		}
		
		options.Clear();
		JDELETE(curOption);
	}
};

#endif // _JTEXTMENU_INCLUDED
