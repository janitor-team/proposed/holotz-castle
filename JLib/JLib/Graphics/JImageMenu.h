/*
 *  JLib - Jacob's Library.
 *  Copyright (C) 2003, 2004  Juan Carlos Seijo P�rez
 * 
 *  This library is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU Library General Public
 *  License as published by the Free Software Foundation; either
 *  version 2 of the License, or (at your option) any later version.
 * 
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Library General Public License for more details.
 * 
 *  You should have received a copy of the GNU Library General Public
 *  License along with this library; if not, write to the Free
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 *  Juan Carlos Seijo P�rez
 *  jacob@mainreactor.net
 */

/** Menu en pantalla compuesto de im�genes.
 * @file    JImageMenu.h
 * @author  Juan Carlos Seijo P�rez
 * @date    28/04/2004
 * @version 0.0.1 - 28/04/2004 - Primera versi�n.
 * @version 0.0.2 - 25/09/2004 - Modificaci�n del m�todo 2D de Pos() para aceptar floats (quita libertad sino).
 */

#ifndef _JIMAGEMENU_INCLUDED
#define _JIMAGEMENU_INCLUDED

#include <JLib/Graphics/JDrawable.h>
#include <JLib/Graphics/JImage.h>
#include <JLib/Util/JApp.h>
#include <JLib/Util/JString.h>
#include <JLib/Util/JTree.h>
#include <JLib/Util/JTimer.h>

/** Distribuci�n del men�.
 */
typedef enum 
{
	JIMAGEMENU_LEFT = 1,                   /**< Alineado a la izquierda. */
	JIMAGEMENU_RIGHT,                      /**< Alineado a la derecha. */
	JIMAGEMENU_CENTER,                     /**< Centrado. */
	JIMAGEMENU_SAMELINE,                   /**< Todas las opciones en la misma l�nea. */
	JIMAGEMENU_FREE,                       /**< Opciones en cualquier posici�n */
} JImageMenuLayout;

/** Estructura de configuraci�n del men�.
 */
struct JImageMenuConfig
{
	JImageMenuLayout layout;              /**< Distribuci�n del men�. */
	bool trackMouse;                      /**< Indica si puede manejarse con rat�n. */
	bool trackKeyboard;                   /**< Indica si puede manejarse con teclado. */
	bool trackJoystick;                   /**< Indica si puede manejarse con joystick/pad. */
	bool autoEnter;                       /**< Indica si desciende si no hay acci�n asociada. */
};

/** Opci�n de men� b�sica.
 */
class JImageMenuEntry
{
 protected:
	JImage *image;                        /**< Imagen asociada a la opci�n. */
	JImage *hiImage;                      /**< Imagen asociada a la opci�n resaltada. */

	void (*Action)(void *data);           /**< Acci�n asociada a la opci�n de men�. */
	void *data;                           /**< Datos asociados a la opci�n de men�. */

 public:
  /** Creates a menu entry.
   * @param  img Image for the item unhighlighted.
   * @param  hiImg Image for the item highlighted.
   * @param  pAct Pointer to function to call when the item is selected.
   * @param  actionData Pointer to additional data to be passed to the action function. In case you want to pass
   * a value (not a pointer to the value) to store it into the pointer itself, you can use the macros
   * JCAST_TSS_TO_VOIDPTR in JLib/Util/JTypes.h, where T in TSS is the signedness of the type (S or U)
   * and SS is the size in bits (8, 16 or 32). Read carefully the comments in that file before using them.
   */
	JImageMenuEntry(JImage *img, JImage *hiImg, void (*pAct)(void *) = 0, void *actionData = 0)
	: image(img), hiImage(hiImg), Action(pAct), data(actionData)
	{}

	/** Selecciona la opci�n de men�, llamando a la callback asociada, si existe.
	 * @return <b>true</b> en caso de haber acci�n asociada, <b>false</b> si no.
	 */
	bool Select();

	/** Devuelve la imagen asociada.
	 * @return La imagen asociada.
	 */
	JImage * Image() {return image;}

	/** Devuelve la imagen asociada al estado resaltado.
	 * @return La imagen asociada al estado resaltado.
	 */
	JImage * HiImage() {return hiImage;}

	/** Destruye el objeto.
	 */
	~JImageMenuEntry()
	{}
};

/** Encapsula un men� de texto en pantalla.
 */
class JImageMenu : public JDrawable
{
	JTree<JImageMenuEntry *> options;       /**< �rbol de opciones de men�. */
	JTree<JImageMenuEntry *>::Iterator *curOption; /**< Opci�n actual. */
	JImageMenuConfig config;              /**< Par�metros de configuraci�n del men�. */
	
	/** Aplica la distribuci�n de men�.
	 */
	void ApplyLayout(JTree<JImageMenuEntry *>::Iterator *it);

 public:
	/** Crea un men� vac�o.
	 */
	JImageMenu();

	/** Inicializa el men� con la configuraci�n dada. Esta funci�n debe
	 * ser llamada despu�s de haber rellenado el �rbol del men�.
	 * @param  cfg Configuraci�n del men�.
	 * @return <b>true</b> Si todo fue bien, <b>false</b> si no se encontr� la
	 * fuente dada.
	 */
	bool Init(JImageMenuConfig &cfg);

	/** Devuelve el iterador de opci�n actual.
	 * @return Iterador de opci�n actual.
	 */
	JTree<JImageMenuEntry *>::Iterator * Menu() {return curOption;}

	/** Establece la posici�n del men�. La alineaci�n se hace respecto de
	 * esta posici�n.
	 * @param  x Posici�n x.
	 * @param  y Posici�n y.
	 */
	virtual void Pos(float x, float y);

	/** Funci�n de dibujo del men�.
	 */
	virtual void Draw();

	/** Procesa tecla arriba
	 */
	virtual void TrackKeyboard(SDL_keysym key);

	/** Procesa movimiento de mouse
	 */
	virtual void TrackMouse(s32 bt, s32 x, s32 y);

	/** Destruye el objeto y libera la memoria asociada.
	 */
	virtual ~JImageMenu()
	{
		for (options.Begin(); options.End(); options.Next())
			JDELETE(options.Cur());
		
		options.Clear();
	}
};

#endif // _JIMAGEMENU_INCLUDED
