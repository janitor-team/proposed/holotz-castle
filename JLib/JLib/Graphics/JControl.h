/*
 *  JLib - Jacob's Library.
 *  Copyright (C) 2003, 2004  Juan Carlos Seijo P�rez
 * 
 *  This library is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU Library General Public
 *  License as published by the Free Software Foundation; either
 *  version 2 of the License, or (at your option) any later version.
 * 
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Library General Public License for more details.
 * 
 *  You should have received a copy of the GNU Library General Public
 *  License along with this library; if not, write to the Free
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 *  Juan Carlos Seijo P�rez
 *  jacob@mainreactor.net
 */

/** Clase base de un control de interfaz de usuario.
 * @file    JControl.h.
 * @author  Juan Carlos Seijo P�rez.
 * @date    27/10/2003.
 * @version 0.0.1 - 27/10/2003 - Primera versi�n.
 */

#ifndef _JCONTROL_INCLUDED
#define _JCONTROL_INCLUDED

#include <JLib/Util/JTypes.h>
#include <JLib/Graphics/JDrawable.h>
#include <string.h>

class JUI;

/** Tipo de control.
 */
typedef enum 
{
	JCONTROLTYPE_IMAGE = 0,                              /**< Im�genes est�ticas. */
	JCONTROLTYPE_IMAGESPRITE,                            /**< Sprite de im�genes. */
	JCONTROLTYPE_MESH,                                   /**< Geometr�a. */
	JCONTROLTYPE_MESHSPRITE,                             /**< Sprite de geometr�a. */
} JControlType;

/** Clase base de un control de interfaz de usuario.
 */
class JControl : public JDrawable
{
	friend class JUI;

private:
  static u32 controlCount;                             /**< Lleva cuenta del n� de controles. */

protected:
	JControlType type;                                   /**< Tipo de control. */
	u32 id;                                              /**< Identificador de control. */
	u32 parentId;                                        /**< Identificador del control padre. */
	JControl * parent;                                   /**< Control padre. */
  u32 focusIndex;                                      /**< �ndice de enfoque. */
	u32 state;                                           /**< Estado del control. */
  void (*focus)(JControl *c, void *data);              /**< Funci�n de enfoque. */
  void (*unfocus)(JControl *c, void *data);            /**< Funci�n de desenfoque. */
  void (*enable)(JControl *c, void *data);             /**< Funci�n de habilitaci�n. */
  void (*disable)(JControl *c, void *data);            /**< Funci�n de deshabilitaci�n. */
  void (*appear)(JControl *c, void *data);             /**< Funci�n de aparici�n. */
  void (*disappear)(JControl *c, void *data);          /**< Funci�n de desaparici�n. */
  void (*select)(JControl *c, void *data);             /**< Funci�n de selecci�n. */
  void (*unselect)(JControl *c, void *data);           /**< Funci�n de deselecci�n. */
  void *focusData;                                     /**< Datos adicionales de enfoque. */
  void *unfocusData;                                   /**< Datos adicionales de desenfoque. */
  void *enableData;                                    /**< Datos adicionales de habilitaci�n. */
  void *disableData;                                   /**< Datos adicionales de deshabilitar. */
  void *appearData;                                    /**< Datos adicionales de aparici�n. */
  void *disappearData;                                 /**< Datos adicionales de desaparici�n. */
  void *selectData;                                    /**< Datos adicionales de selecci�n. */
  void *unselectData;                                  /**< Datos adicionales de deselecci�n. */
	
  /** Acci�n a realizar cuando recibe el enfoque. */
  void OnFocus() {if (focus) focus(this, focusData);};
	
  /** Acci�n a realizar cuando pierde el enfoque. */
  void OnUnfocus() {if (unfocus) unfocus(this, unfocusData);};
	
  /** Acci�n a realizar cuando es activado. */
  void OnEnable() {if (enable) enable(this, enableData);};
	
  /** Acci�n a realizar cuando es desactivado. */
  void OnDisable() {if (disable) disable(this, disableData);};
	
  /** Acci�n a realizar cuando aparece. */
  void OnAppear() {if (appear) appear(this, appearData);};
  
  /** Acci�n a realizar cuando desaparece. */
  void OnDisappear() {if (disappear) disappear(this, disappearData);};
	
  /** Acci�n a realizar cuando se selecciona. */
  void OnSelect() {if (select) select(this, selectData);};
  
  /** Acci�n a realizar cuando se deselecciona. */
  void OnUnselect() {if (unselect) unselect(this, unselectData);};
	
 public:
  static const u32 VISIBLE;    /**< Flag de visibilidad. */
  static const u32 FOCUSED;    /**< Flag de enfoque. */
  static const u32 ENABLED;    /**< Flag de habilitaci�n. */
  static const u32 SELECTED;   /**< Flag de selecci�n. */
	
  /** Crea un control vac�o.
   */
  JControl(JControl *_parent = 0, u32 _state = VISIBLE & ENABLED);
  
	/** Devuelve el identificador de este control.
	 * @return Identificador del control.
	 */
	u32 Id() {return id;}
	
	/** Devuelve el control padre.
	 * @return Control padre de este, 0 si no tiene.
	 */
	JControl * Parent() {return parent;}
	
  /** Funci�n de dibujo. */
  virtual void Draw();
	
  /** Funci�n de actualizaci�n cuando est� visible.
   * @return Dependiente de la implementaci�n.
   */
  virtual s32 Update();
	
  /** Funci�n de dibujo cuando est� �nicamente visible. */
  virtual void DrawVisible() = 0;
	
  /** Funci�n de dibujo cuando est� enfocado. */
  virtual void DrawFocus() = 0;
	
  /** Funci�n de dibujo cuando est� deshabilitado. */
  virtual void DrawDisabled() = 0;

  /** Funci�n de dibujo cuando est� seleccionado. */
  virtual void DrawSelected() = 0;

  /** Funci�n de actualizaci�n cuando est� �nicamente visible. 
   * @return M�scara de estado del control.
	 */
  virtual s32 UpdateVisible() = 0;

  /** Funci�n de actualizaci�n cuando est� enfocado. 
   * @return Dependiente de la implementaci�n.
   */
  virtual s32 UpdateFocus() = 0;

  /** Funci�n de actualizaci�n cuando est� deshabilitado. 
   * @return Dependiente de la implementaci�n.
   */
  virtual s32 UpdateDisabled() = 0;

  /** Funci�n de actualizaci�n cuando est� seleccionado. 
   * @return Dependiente de la implementaci�n.
   */
  virtual s32 UpdateSelected() = 0;

  /** Muestra el control.
   */
  virtual void Appear();

  /** Oculta el control.
   */
  virtual void Disappear();
  
  /** Habilita el control.
   */
  virtual void Enable();

  /** Deshabilita el control.
   */
  virtual void Disable();

  /** Enfoca el control.
   */
  virtual void Focus();

  /** Desenfoca el control.
   */
  virtual void Unfocus();

  /** Selecciona el control.
   */
  virtual void Select();

  /** Deselecciona el control.
   */
  virtual void Unselect();

  /** Establece la funci�n para cuando recibe el enfoque.
   * @param  func Funci�n callback a llamara cuando se produce el evento.
   * @param  data Datos adicionales.
   */
  void SetOnFocus(void (*func)(JControl*, void *), void *data = 0)     {focus = func; focusData = data;};

  /** Establece la funci�n para cuando pierde el enfoque.
   * @param  func Funci�n callback a llamara cuando se produce el evento.
   * @param  data Datos adicionales.
   */
  void SetOnUnfocus(void (*func)(JControl*, void *), void *data = 0)   {unfocus = func; unfocusData = data;};
  
  /** Establece la funci�n para cuando es activado.
   * @param  func Funci�n callback a llamara cuando se produce el evento.
   * @param  data Datos adicionales.
   */
  void SetOnEnable(void (*func)(JControl*, void *), void *data = 0)    {enable = func; enableData = data;};

  /** Establece la funci�n para cuando es desactivado.
   * @param  func Funci�n callback a llamara cuando se produce el evento.
   * @param  data Datos adicionales.
   */
  void SetOnDisable(void (*func)(JControl*, void *), void *data = 0)   {disable = func; disableData = data;};

  /** Establece la funci�n para cuando aparece.
   * @param  func Funci�n callback a llamara cuando se produce el evento.
   * @param  data Datos adicionales.
   */
  void SetOnAppear(void (*func)(JControl*, void *), void *data = 0)    {appear = func; appearData = data;};
  
  /** Establece la funci�n para cuando desaparece.
   * @param  func Funci�n callback a llamara cuando se produce el evento.
   * @param  data Datos adicionales.
   */
  void SetOnDisappear(void (*func)(JControl*, void *), void *data = 0) {disappear = func; disappearData = data;};

  /** Establece la funci�n para cuando se selecciona.
   * @param  func Funci�n callback a llamara cuando se produce el evento.
   * @param  data Datos adicionales.
   */
  void SetOnSelect(void (*func)(JControl*, void *), void *data = 0)    {select = func; selectData = data;};
  
  /** Establece la funci�n para cuando se deselecciona.
   * @param  func Funci�n callback a llamara cuando se produce el evento.
   * @param  data Datos adicionales.
   */
  void SetOnUnselect(void (*func)(JControl*, void *), void *data = 0)  {unselect = func; unselectData = data;};

	/** Determina si el control est� visible.
   * @return <b>true</b> si lo est�, <b>false</b> si no.
   */
	bool Visible() {return 0 != (state & VISIBLE);}

	/** Determina si el control est� enfocado.
   * @return <b>true</b> si lo est�, <b>false</b> si no.
   */
	bool Focused() {return 0 != (state & FOCUSED);}

	/** Determina si el control est� seleccionado.
   * @return <b>true</b> si lo est�, <b>false</b> si no.
   */
	bool Selected() {return 0 != (state & SELECTED);}

	/** Determina si el control est� activado.
   * @return <b>true</b> si lo est�, <b>false</b> si no.
   */
	bool Enabled() {return 0 != (state & ENABLED);}

	/** Carga el control desde un fichero.
   * @param  f Fichero abierto y posicionado para cargar el objeto.
   * @return 0 si todo va bien, 1 en caso de error de E/S,
   * 2 en caso de incoherencia de los datos.
	 */
	u32 Load(JRW &f);

	/** Salva la interfaz a un fichero de definici�n de interfaz.
   * @param  f Fichero abierto y posicionado para salvar el objeto.
   * @return 0 si todo va bien, 1 en caso de error de E/S,
   * 2 en caso de incoherencia de los datos.
	 */
	u32 Save(JRW &f);

  /** Libera la memoria asociada.
   */
  virtual ~JControl();
};

#endif  // _JCONTROL_INCLUDED
