/*
 *  JLib - Jacob's Library.
 *  Copyright (C) 2003, 2004  Juan Carlos Seijo P�rez
 * 
 *  This library is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU Library General Public
 *  License as published by the Free Software Foundation; either
 *  version 2 of the License, or (at your option) any later version.
 * 
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Library General Public License for more details.
 * 
 *  You should have received a copy of the GNU Library General Public
 *  License along with this library; if not, write to the Free
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 *  Juan Carlos Seijo P�rez
 *  jacob@mainreactor.net
 */

/** Pol�gono cerrado gen�rico
 * @file J2DPolygon.h
 * @author Juan Carlos Seijo P�rez
 * @date 01/02/2004 
 */

#ifndef _J2DPOLYGON_INCLUDED
#define _J2DPOLYGON_INCLUDED

#include <JLib/Util/JTypes.h>
#include <JLib/Util/JLoadSave.h>
#include <JLib/Math/J2DVector.h>
#include <string.h>
#include <math.h>

/** Pol�gono cerrado gen�rico.
 * Consta de varios v�rtices conectados correlativamente.
 */
class J2DPolygon : public JLoadSave
{
protected:
	s32 numVertices;                      /**< N�mero de v�rtices del pol�gono */
	J2DPoint* vertices;                   /**< V�rtices del pol�gono */
	J2DVector* segments;                  /**< Segmentos del pol�gono */
	J2DVector* normals;                   /**< Normales a los segmentos del pol�gono */

public:
	/** Constructor.
	 * Crea un pol�gono vac�o.
	 */
	J2DPolygon() : numVertices(0), vertices(0), segments(0), normals(0) {}
	
	
	/** Constructor.
	 * Crea un pol�gono cerrado a partir de los v�rtices dados.
	 * @param _numVertices N�mero de v�rtices.
	 * @param _vertices V�rtices.
	 */
	J2DPolygon(s32 _numVertices, J2DPoint *_vertices) : numVertices(0), vertices(0), segments(0), normals(0)
	{Init(_numVertices, _vertices);}

	/** Inicializa el pol�gono con los v�rtices dados.
	 * @param _numVertices N�mero de v�rtices.
	 * @param _vertices V�rtices.
	 */
	void Init(s32 _numVertices, J2DPoint *_vertices);
	
	/** Destructor.
	 * Libera memoria.
	 */
	virtual ~J2DPolygon() {Destroy();}
	
	/** Destruye el objeto y lo deja como si se acabar� de crear con J2DPolygon.
	 */
	void Destroy();

	/** Determina si el punto dado est� dentro del pol�gono.
	 * Por convenio, si un pol�gono est� definido en 
	 * sentido antihorario (CCW) entonces un punto est� dentro si cae dentro del 
	 * contorno cerrado del pol�gono. Si se define en sentido horario (CW), los puntos 
	 * de dentro del contorno cerrado del pol�gono est�n fuera.
	 * @param x coordenada x del punto.
	 * @param y coordenada y del punto.
	 * @return true si el punto dado est� dentro, false en caso contrario.
	 */
	bool IsInside(J2DScalar x, J2DScalar y);

  /** Carga el pol�gono
	 * @param f Fichero posicionado en este recurso
   * @return 0 Si todo fue bien, 1 si hay error de E/S y 2 si hay error
   * en los datos.
	 */
	u32 Load(JFile &f);

  /** Salva el pol�gono
	 * @param f Fichero posicionado en este recurso
   * @return 0 Si todo fue bien, 1 si hay error de E/S y 2 si hay error
   * en los datos.
	 */
	u32 Save(JFile &f);
};

#endif // _J2DPOLYGON_INCLUDED
