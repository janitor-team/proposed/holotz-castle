holotz-castle (1.3.14-12) unstable; urgency=medium

  * Team upload.
  * Declare compliance with Debian Policy 4.6.0.
  * Remove B-D on dpkg-dev.
  * Mark holotz-castle-data Multi-Arch: foreign
  * Fix FTBFS with GCC 11. (Closes: #984177)

 -- Markus Koschany <apo@debian.org>  Thu, 21 Oct 2021 23:45:09 +0200

holotz-castle (1.3.14-11) unstable; urgency=medium

  * Team upload.
  * Build with --no-parallel to avoid a race condition and FTBFS.

 -- Markus Koschany <apo@debian.org>  Sun, 31 May 2020 15:46:25 +0200

holotz-castle (1.3.14-10) unstable; urgency=medium

  * Team upload.
  * Fix FTCBFS: upstream build system hard codes build architecture compiler.
    Thanks to Helmut Grohne for the patch. (Closes: #900141)
  * Switch to debhelper-compat = 13.
  * Declare compliance with Debian Policy 4.5.0.
  * Remove obsolete holotz-castle.menu file.

 -- Markus Koschany <apo@debian.org>  Sat, 30 May 2020 22:07:03 +0200

holotz-castle (1.3.14-9) unstable; urgency=medium

  * Team upload.
  * Split maintscript file into $PACKAGE.maintscript files. (Closes: #857845)
    Thanks to Andreas Beckmann for the report.

 -- Markus Koschany <apo@debian.org>  Wed, 15 Mar 2017 19:23:28 +0100

holotz-castle (1.3.14-8) unstable; urgency=medium

  * Team upload.
  * Remove override for dh_installdocs and do not use unsupported --link-doc
    option for arch:all packages. Add maintscript file and use symlink_to_dir
    function to convert symlinked directories to real directories. Thanks to
    Axel Beckert for the report. (Closes: #857236)

 -- Markus Koschany <apo@debian.org>  Sun, 12 Mar 2017 21:50:38 +0100

holotz-castle (1.3.14-7) unstable; urgency=medium

  * Team upload.
  * Vcs-Browser: Use canonical Vcs-URI and https.
  * wrap-and-sort -sa.
  * Declare compliance with Debian Policy 3.9.6.
  * Add keywords and a comment in German.
  * Change homepage address to Debian Wiki because the original website is
    gone.
  * Update debian/copyright to copyright format 1.0. Add missing LGPL-2
    licensed files.
  * Add 15_clang-FTBFS.patch. Fix tautological comparison build failures
    with the clang compiler. Thanks to Alexander for the report and patch.
    (Closes: #754226)
  * Add hardening.patch and pass all build flags to the build system.
  * Remove Suggests: holotz-castle-milanb because this package does not exist
    anymore. Milanb is already part of holotz-castle.

 -- Markus Koschany <apo@debian.org>  Thu, 26 Nov 2015 16:09:36 +0100

holotz-castle (1.3.14-6) unstable; urgency=low

  * Add patch to fix FTBFS with gcc-4.8. (Closes: #701294).
    + Thanks to Juhani Numminen for the patch.
  * Don't install themes/dungeons as they are just duplicates of themes/default.
  * Add patch to fix spelling errors in manpage and binaries.
  * Bump debhelper build-dep and compat to 9.
  * Bump Standards Version to 3.9.4.

 -- Barry deFreese <bdefreese@debian.org>  Fri, 07 Jun 2013 16:47:25 -0400

holotz-castle (1.3.14-5) unstable; urgency=low

  * Team upload.
  * Build-Depends on zlib1g-dev. Fix FTBFS (Closes: #669444)
  * Standards-Version 3.9.3

 -- Vincent Legout <vlegout@debian.org>  Sun, 29 Apr 2012 10:17:42 +0200

holotz-castle (1.3.14-4) unstable; urgency=low

  * Team upload

  [ Felix Geyer ]
  * Fix FTBFS with ld --as-needed.
    - Update 02_makefile.patch
  * Update Vcs-Browser control field.
  * Drop executable bit from data files.
  * Bump Standards-Version to 3.9.2, no changes needed.

  [ Andreas Moog ]
  * Fix compiler warnings -Wunused-but-set-variable and -Wunused-result
    (Closes: #625353)
    - Add 12_gcc-4.6.patch
  * Switch to dh-sequencer in debian/rules (Closes: #633494)
    - Use debian/*.install|manpages
    - Switch to compat level 8

 -- Felix Geyer <debfx-pkg@fobos.de>  Thu, 04 Aug 2011 11:10:01 +0200

holotz-castle (1.3.14-3) unstable; urgency=low

  * Team upload.

  [ Peter Pentchev ]
  * Link with zlib to fix the FTBFS with recent linker versions.
    Closes: #615730
  * Convert to the 3.0 (quilt) source format:
    - drop the quilt build dependency
    - drop the README.source file
    - drop the quilt snippets in the rules file
  * Add misc:Depends to the binary packages.
  * Add a DEP 3 header to the 11_dirent64 patch.

  [ Ansgar Burchardt ]
  * debian/copyright: Refer to "Debian systems" instead of only "Debian
    GNU/Linux systems".
  * Bump Standards-Version to 3.9.1 (no changes).

 -- Ansgar Burchardt <ansgar@debian.org>  Wed, 16 Mar 2011 11:46:03 +0100

holotz-castle (1.3.14-2) unstable; urgency=low

  [ Barry deFreese ]
  * 11_dirent64.patch-JFS.cpp change dirent64 to dirent. (Closes: #552900).
  * Add README.source for quilt.
  * Bump Standards Version to 3.8.3. (No changes needed).

 -- Barry deFreese <bdefreese@debian.org>  Fri, 13 Nov 2009 09:12:48 -0500

holotz-castle (1.3.14-1) unstable; urgency=low

  [ Barry deFreese ]
  * New upstream release.

 -- Barry deFreese <bdefreese@debian.org>  Fri, 15 May 2009 11:59:12 -0400

holotz-castle (1.3.13-1) unstable; urgency=low

  [ Miriam Ruiz ]
  * New Upstream Release
  * milanb levels are now included in the main source. Added
    holotz-castle-milanb to Conflicts and Provides
  * Removed DM-Upload-Allowed: yes from debian/control
  * Refreshed patches so that they apply on the newer code
  * Manpages are now in man/ and not in doc/

  [ Barry deFreese ]
  * Add myself to uploaders.
  * Version GPL license path to GPL-2.
  * 10_dont_hardcode_g++41.patch - JLib/Makefile hardcoded to g++-4.1.
  * Bump Standards Version to 3.8.1. (No changes needed).

 -- Barry deFreese <bdefreese@debian.org>  Wed, 08 Apr 2009 12:42:18 -0400

holotz-castle (1.3.10-1) unstable; urgency=high

  [ Eddy Petrișor ]
  * remove myself from the Uploaders list

  [ Jon Dowland ]
  * update menu section to "Games/Action" for menu transition
    (thanks Linas Žvirblis)

  [ Cyril Brulebois ]
  * Added Vcs-Svn and Vcs-Browser fields in the control file.

  [ Barry deFreese ]
  * New upstream release.
  * Update copyright file. (Closes: #457257).
  * Add suggests for holotz-castle-milanb. (Closes: #455596).
  * Add watch file.
  * Bump debhelper build-dep to 5.
  * Add Homepage field in control.
  * Add typo fixes in manpage patches.
  * Remove deprecated encoding tag from desktop file.
  * Convert encoding to UTF-8 on debian/copyright.
  * Bump Standards Version to 3.7.3.
    + Menu policy transition.

  [ Ansgar Burchardt ]
  * debian/control: Change XS-Vcs-* to Vcs-*

  [ Miriam Ruiz ]
  * Updated 05_jlib.patch. Fixed FTBFS with GCC 4.3. Closes: #417061
    Thanks to Maximiliano Curia <maxy@gnuservers.com.ar>
  * Added patch to fix gcc 4.3 issues: 09_gcc-4.3.patch
  * Added copyright statements to patches

 -- Debian Games Team <pkg-games-devel@lists.alioth.debian.org>  Sat, 26 Apr 2008 02:25:38 +0200

holotz-castle (1.3.9-1) unstable; urgency=low

  [ Gonéri Le Bouder ]
  * call dh_desktop to run update-desktop-database

  [Miriam Ruiz]
  * Added debian/docs list of files
  * New Upstream Release:
    + Added translation to Portuguese
    + Updated translation to English

 -- Miriam Ruiz <little_miry@yahoo.es>  Wed, 14 Mar 2007 08:54:05 +0100

holotz-castle (1.3.8-2) unstable; urgency=low

  [Eddy Petrișor]
  * Acknowledging NMU, thanks (Closes: #357897)
  * Merged patch for .desktop present in Ubuntu to be more freedesktop
    compilant. Thanks Barry deFreese (Closes: #376887)
  * Added Romanian translation of the desktop file fields.
  * Added myself to uploaders.

  [Miriam Ruiz]
  * Modified building system for using quilt.
  * Updated to Standards Version 3.7.2. No changes needed.
  * Added 08_jlib_jtree.patch to replace ~JTree<T>() by ~JTree(). The
    former seems to give a "parse error in template argument list" in
    gcc-4.1 4.1.1-16 (even though 4.1.1-13 accepts it). Closes: #392172
  * Added Spanish translation of the desktop file fields.
  * Made package safely binNMUable by replacing (= ${Source-Version})
    with (= ${source:Version}) in debian/control.

  [ Gonéri Le Bouder ]
  * remove Applications from Categorie in the desktop file since it's not
    an official categorie

 -- Miriam Ruiz <little_miry@yahoo.es>  Thu, 12 Oct 2006 19:47:22 +0200

holotz-castle (1.3.8-1.1) unstable; urgency=low

  * Non-maintainer upload.
  * Fix warnings (treated as errors because of -Werror) about questionable
    casts; fixes FTBFS with g++ 4.1, patch from Ben Hutchings.
    (Closes: #357897)

 -- Steinar H. Gunderson <sesse@debian.org>  Wed,  7 Jun 2006 21:18:09 +0200

holotz-castle (1.3.8-1) unstable; urgency=low

  * New Upstream Version.
  * Changed Maintainer field to Debian Games Team.

 -- Miriam Ruiz <little_miry@yahoo.es>  Thu, 26 Jan 2006 22:37:44 +0100

holotz-castle (1.3.7-1) unstable; urgency=low

  * New Upstream Version.
  * Added .desktop file shortcut Closes: #305333.
  * Updated to Standards Version 3.6.2
  * Changed FSF address in debian/copyright
  * Removed dependency from data files to game program to avoid circuler
    dependencies.

 -- Miriam Ruiz <little_miry@yahoo.es>  Thu, 17 Nov 2005 07:37:59 +0100

holotz-castle (1.3.6-1) unstable; urgency=low

  * New Upstream Version.
  * Solves endianess problems and works in PowerPCs now. Closes: #304715.

 -- Miriam Ruiz <little_miry@yahoo.es>  Tue, 11 May 2005 00:37:00 +0100

holotz-castle (1.3.4-3) unstable; urgency=low

  * Fixed the code to solve endianess problems. It should now work in powerpc.
    Closes: #304715.
  * Thanks to Nacho Barrientos Arias <chipi@criptonita.com> for his help in
    testing the changes on a powerpc.

 -- Miriam Ruiz <little_miry@yahoo.es>  Mon, 18 Apr 2005 00:18:00 +0100

holotz-castle (1.3.4-2) unstable; urgency=low

  * Execute debhelper on arch-specific packages only in the binary-arch
    target. Closes: #304132.
  * Applied patch to solve compilation problem in amd64 architecture.
    Thanks to Andreas Jochens for the patch. Closes #303997.

 -- Miriam Ruiz <little_miry@yahoo.es>  Mon, 11 Apr 2005 16:48:00 +0100

holotz-castle (1.3.4-1) unstable; urgency=low

  * New upstream Version
  * Added Euskara translation.

 -- Miriam Ruiz <little_miry@yahoo.es>  Mon, 4 Apr 2005 17:02:00 +0100

holotz-castle (1.3.3-1) unstable; urgency=low

  * New upstream Version
  * Added UTF-8 support. Now it can be translated to any language.
  * Added Russian translation.
  * Fixed a bug that got the character stuck in the floor.

 -- Miriam Ruiz <little_miry@yahoo.es>  Mon, 21 Mar 2005 18:58:12 +0100

holotz-castle (1.3.1-1) unstable; urgency=low

  * New upstream Version

 -- Miriam Ruiz <little_miry@yahoo.es>  Tue, 8 Feb 2005 16:02:00 +0100

holotz-castle (1.3-1) unstable; urgency=low

  * Initial Release (Closes: #294845)

 -- Miriam Ruiz <little_miry@yahoo.es>  Tue, 25 Jan 2005 06:36:21 +0100

