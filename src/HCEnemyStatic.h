/*
 * Holotz's Castle
 * Copyright (C) 2004 Juan Carlos Seijo P�rez
 * 
 * This program is free software; you can redistribute it and/or modify it 
 * under the terms of the GNU General Public License as published by the Free 
 * Software Foundation; either version 2 of the License, or (at your option) 
 * any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT 
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or 
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for 
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along 
 * with this program; if not, write to the Free Software Foundation, Inc., 59 
 * Temple Place, Suite 330, Boston, MA 02111-1307 USA
 * 
 * Juan Carlos Seijo P�rez
 * jacob@mainreactor.net
 */

/** Defines a static enemy.
 * @file    HCEnemyStatic.h
 * @author  Juan Carlos Seijo P�rez
 * @date    27/05/2004
 * @version 0.0.1 - 27/05/2004 - First version.
 */

#ifndef _HCENEMYSTATIC_INCLUDED
#define _HCENEMYSTATIC_INCLUDED

#include <HCEnemy.h>
#include <HCLevel.h>

/** Static enemy. Only stays (kind of trap).
 */
class HCEnemyStatic : public HCEnemy
{
 public:
	/** Creates an enemy static. 
	 */
	HCEnemyStatic();

	/** Updates the enemy.
	 * @return Return value of the associated drawable's update.
	 */
	virtual s32 Update() {return states[state].Update();}

	/** Destroys the object and allows scalar destruction.
	 */
	virtual ~HCEnemyStatic() {}
};

#endif // _HCENEMYSTATIC_INCLUDED
